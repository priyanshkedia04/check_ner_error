import json
import requests
import difflib
from lattice.lattice import Lattice
import s3fs
from collections import deque, defaultdict
from load_data_s3 import LoadData
import os

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
fs = s3fs.S3FileSystem()


def find_paths(directory):
    final_paths = []
    q = deque()
    for s3_path in fs.ls(directory):
        q.append(s3_path)

    while q:
        current_path = q.pop()
        if '.gz' in current_path:
            final_paths.append(current_path)
        else:
            for s3_path in fs.ls(current_path):
                q.append(s3_path)
    print(len(final_paths))
    return final_paths


def construct_dataframe(final_paths):
    ld = LoadData()
    df = ld.parallelize_func(final_paths, ld.load_data_froms3)
    print(df.shape)

    map_sms2ml = dict(zip(df.body, df.ml_attributes))
    print(len(map_sms2ml))

    with open('map_sms2ml.json', 'w') as f:
        json.dump(map_sms2ml, f)

    return df


def construct_lattice(data):
    l = Lattice(list(data.body))
    l_df = l.pandas()
    l_df = l_df.sort_values(by='pattern_importance_score', ascending=False)
    l_df = l_df[['pattern', 'original_descriptions']]
    print(l_df.shape)
    # l_df.head()
    map_pattern_sms = {}
    for i in range(len(l_df)):
        map_pattern_sms[l_df.pattern[i]] = list(l_df.original_descriptions[i])[:10]

    with open('map_pattern_sms.json', 'w') as f:
        json.dump(map_pattern_sms, f)


def read_store_data(directory):
    final_paths = find_paths(directory)
    data = construct_dataframe(final_paths)
    construct_lattice(data)


def return_dict():
    d = {"cleaned_sms": None, "normalized_sms": None,
         "received_at": "1574936765104"}
    return d


def ner_error():
    print('Started...')
    url_classification = "http://enigma_ledger_classifier.stg.dreamplug.net/v1/classifier"
    debit_ner = "http://enigma_ledger_debit_ner.stg.dreamplug.net/v1/debit_ner"
    credit_ner = "http://enigma_ledger_credit_ner.stg.dreamplug.net/v1/credit_ner"
    with open('map_pattern_sms.json') as f:
        map_pattern_sms = json.load(f)

    with open('map_sms2ml.json') as f:
        map_sms2ml = json.load(f)

    sentences = []
    for pattern in map_pattern_sms:
        sentences.extend(map_pattern_sms[pattern][0:1])

    print("Total SMS: ", len(sentences))

    print("Working on Batches of 150")
    print('-*-' * 30)

    total_credit, total_debit, credit_ner_error, debit_ner_error, classification_error = 0, 0, 0, 0, 0
    others = 0

    final_output_debit = defaultdict(list)
    final_output_credit = defaultdict(list)
    final_output_classification = []

    for k in range(0, len(sentences), 150):
        print("Batch: ", k // 150 + 1)
        batch_sentences = sentences[k:k + 150]
        resp = requests.post(url_classification, json=batch_sentences)
        classification = resp.json()

        credit_json = []
        debit_json = []
        credit_idx = []
        debit_idx = []

        for i in range(len(classification)):
            output = classification[i]
            orig_sentence = batch_sentences[i]
            ml_attributes = map_sms2ml[orig_sentence]

            if output['sms_type'] == 'Others':
                others += 1
                continue

            if output['sms_type'] != ml_attributes['classification']['sms_type']:
                classification_error += 1
                dt = {'SMS': orig_sentence, 'API': output['sms_type'],
                      'DataFrame': ml_attributes['classification']['sms_type']}
                final_output_classification.append(dt)
                continue

            d = return_dict()
            d['cleaned_sms'] = output['cleaned_sms']
            d['normalized_sms'] = output['normalized_sms']

            if output['sms_type'] == 'Credit':
                credit_json.append(d)
                credit_idx.append(i)
            else:
                debit_json.append(d)
                debit_idx.append(i)

        credit_ent, debit_ent = [], []
        if len(debit_json) != 0:
            resp = requests.post(debit_ner, json=debit_json)
            if resp.status_code == 200:
                debit_ent = resp.json()
                total_debit += len(debit_ent)
            else:
                print('Response Code Error Debit', len(debit_json))
                print(resp.status_code)
        if len(credit_json) != 0:
            resp = requests.post(credit_ner, json=credit_json)
            if resp.status_code == 200:
                credit_ent = resp.json()
                total_credit += len(credit_ent)
            else:
                print('Response Code Error Credit', len(credit_json))
                print(resp.status_code)

        # print("For Credit SMS")

        for i in range(len(credit_ent)):
            out = credit_ent[i]['ner_model']['entities']
            orig = map_sms2ml[batch_sentences[credit_idx[i]]]['ner_model']['entities']
            changes = {'limit_cur': 'used_cur', 'limit_amount': 'used_limit', 'beneficiary': 'description'}
            for ent in changes:
                if ent in orig:
                    val = orig[ent]
                    orig[changes[ent]] = val
                    del orig[ent]
            if 'transaction_time' in orig:
                if 'ist' in orig['transaction_time'][0]:
                    orig['transaction_time'][0] = orig['transaction_time'][0][:-4]

            if "description" in out and "description" in orig:
                ratio = difflib.SequenceMatcher(None, orig['description'], out['description']).ratio()
                if ratio >= 0.7:
                    orig['description'] = out['description']

            if out != orig:
                credit_ner_error += 1
                dt = {'SMS': batch_sentences[credit_idx[i]], 'API': out, 'DataFrame': orig}
                for _key in orig:
                    if _key not in out:
                        final_output_credit[_key].append(dt)
                    else:
                        if orig[_key] != out[_key]:
                            final_output_credit[_key].append(dt)

        for i in range(len(debit_ent)):
            out = debit_ent[i]['ner_model']['entities']
            orig = map_sms2ml[batch_sentences[debit_idx[i]]]['ner_model']['entities']
            changes = {'limit_cur': 'used_cur', 'limit_amount': 'used_limit', 'beneficiary': 'description'}
            for ent in changes:
                if ent in orig:
                    val = orig[ent]
                    orig[changes[ent]] = val
                    del orig[ent]
            if 'transaction_time' in orig:
                if 'ist' in orig['transaction_time'][0]:
                    orig['transaction_time'][0] = orig['transaction_time'][0][:-4]

            if "description" in out and "description" in orig:
                ratio = difflib.SequenceMatcher(None, orig['description'], out['description']).ratio()
                if ratio >= 0.7:
                    orig['description'] = out['description']

            if out != orig:
                debit_ner_error += 1
                dt = {'SMS': batch_sentences[debit_idx[i]], 'API': out, 'DataFrame': orig}
                for _key in orig:
                    if _key not in out:
                        final_output_debit[_key].append(dt)
                    else:
                        if orig[_key] != out[_key]:
                            final_output_debit[_key].append(dt)

    with open('data_credit.json', 'w', encoding='utf-8') as f:
        json.dump(final_output_credit, f, ensure_ascii=False, indent=4)

    with open('data_debit.json', 'w', encoding='utf-8') as f:
        json.dump(final_output_debit, f, ensure_ascii=False, indent=4)

    with open('classification.json', 'w', encoding='utf-8') as f:
        json.dump(final_output_classification, f, ensure_ascii=False, indent=4)

    print("Total SMS: ", len(sentences))
    print("Total Credit SMS: ", total_credit)
    print("Total Debit SMS: ", total_debit)
    print("Total Others: ", others)
    print("Total Classification Error: ", classification_error)
    print("Total Credit NER Error: ", credit_ner_error)
    print("Total Debit NER Error: ", debit_ner_error)
    print("Total NER Error (Credit + Debit): ", credit_ner_error + debit_ner_error)
    print("Total NER Error % :", ((credit_ner_error + debit_ner_error) / len(sentences)) * 100)


def main():
    read_store_data(directory)
    ner_error()


directory = 's3://prod-enigma/enriched_data_v3/ledger/2021/06/30/15/'
main()
