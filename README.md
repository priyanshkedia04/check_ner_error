# README #

This repository contains code to run the Enigma Ledger Classification and NER API and flag where the API is making Errors


### What is this repository for? ###

* The code will first collect descriptions (SMS) and its ml_attributes from the tables in the s3//: directory specified example ('s3://prod-enigma/enriched_data_v3/ledger/2021/06/28/06/'). Each SMS with its ml_attribute will be stored in a JSON file.
* The descriptions will then be passed through Lattice to get most common SMS patterns and correspong to each pattern we will be taking 10 descriptions for checking NER error. These 10 descriptions along with their pattern will be stored in a JSON file.
* The descriptions has now been divided based on their patterns. 2-3 descriptions from each pattern will then be passed through the Enigma Ledger Classification API, to check if it is classified correctly, if not then that SMS will be flagged and stored in classification.json.
* We have called all the APIs with a batch_size of 150.
* The correctly classified descriptions will then be passed through Enigma Ledger Debit NER or Credit NER depending on its class. 
* All the predicted credit/debit entities for each description will be cross-checked with the original entities we had in the ml_attributes (We stored this info earlier in a JSON file.)
* SMS having NER errors will be flagged and stored in a json file along with the entity that is showing error.

### Who do I talk to? ###

* Priyansh Kedia