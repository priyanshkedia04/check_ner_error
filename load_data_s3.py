import multiprocessing as mp
import pandas as pd
import numpy as np
import boto3
import os

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')

s3_get_object = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
num_part = mp.cpu_count() - 1


class LoadData:

    def get_file_from_s3(self, bucket, path):
        obj = s3_get_object.get_object(Bucket=bucket, Key=path)
        return obj['Body']

    def load_data_froms3(self, file_list):
        data_frames = []
        for path in file_list:
            p = path[12:]
            temp = pd.read_json(self.get_file_from_s3("prod-enigma", p), compression='gzip', lines=True)
            temp = temp[['body', 'ml_attributes']]
            data_frames.append(temp)
        return pd.concat(data_frames)

    def parallelize_func(self, files, func):
        n_p = min(len(files), num_part)
        pool = mp.Pool(n_p)
        file_split = np.array_split(files, n_p)
        df = pd.concat(pool.map(func, file_split))
        pool.close()
        pool.join()
        return df
